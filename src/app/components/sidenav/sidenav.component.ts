import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent implements OnInit {
  sidenavOpened = true;
  links = [
    {
      route: 'url',
      name: 'URL Converter',
      icon: '',
    },
    {
      route: 'domains',
      name: 'Popular Domains',
      icon: '',
    },
  ];
  constructor() {}

  ngOnInit(): void {}

  openRepo() {}
}
