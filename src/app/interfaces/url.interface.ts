export interface IShortenedUrl {
  tinyUrl: string;
  longUrl: string;
}
