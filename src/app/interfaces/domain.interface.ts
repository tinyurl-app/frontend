export interface IDomain {
  name: string;
  count: number;
  createdAt: Date;
  updatedAt: Date;
  id: string;
}
