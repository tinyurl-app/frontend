import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopularDomainsComponent } from './popular-domains.component';
import { MatListModule } from '@angular/material/list';

@NgModule({
  declarations: [PopularDomainsComponent],
  imports: [CommonModule, MatListModule],
  exports: [PopularDomainsComponent],
})
export class PopularDomainsModule {}
