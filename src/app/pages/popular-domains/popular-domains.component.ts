import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IDomain } from 'src/app/interfaces/domain.interface';
import { DomainService } from 'src/app/services/domain.service';

@Component({
  selector: 'app-popular-domains',
  templateUrl: './popular-domains.component.html',
  styleUrls: ['./popular-domains.component.scss'],
})
export class PopularDomainsComponent implements OnInit {
  domains$: Observable<IDomain[]>;
  constructor(private domainService: DomainService) {}

  ngOnInit(): void {
    this.domains$ = this.domainService.getPopularDomains();
  }
}
