import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-url-input',
  templateUrl: './url-input.component.html',
  styleUrls: ['./url-input.component.scss'],
})
export class UrlInputComponent implements OnInit {
  form: FormControl;
  @Output() url = new EventEmitter<FormControl>();
  constructor() {
    this.form = new FormControl(null, UrlValidator);
  }

  ngOnInit(): void {
    this.url.emit(this.form);
  }
}

function UrlValidator(control: AbstractControl): null | ValidationErrors {
  try {
    const value = control.value;
    value && new URL(control.value);
    return null;
  } catch {
    return { urlPattern: true };
  }
}
