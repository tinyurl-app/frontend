import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UrlService } from 'src/app/services/url.service';

@Component({
  selector: 'app-generate-url',
  templateUrl: './generate-url.component.html',
  styleUrls: ['./generate-url.component.scss'],
})
export class GenerateUrlComponent implements OnInit {
  urlForm: FormControl;
  tinyUrl: string;
  longUrl: string;
  error: string;
  constructor(private urlService: UrlService) {}

  ngOnInit(): void {}

  get showConvert() {
    return !this.tinyUrl;
  }

  get showReset() {
    return !!this.tinyUrl || !!this.error;
  }

  convertUrl() {
    const longUrl = this.urlForm.value.trim();
    this.urlService.transformUrl(longUrl).subscribe({
      next: (url) => {
        this.tinyUrl = url;
        this.longUrl = longUrl;
        this.error = null;
      },
      error: () => {
        this.error =
          'An error ocurred and the URL could not be converted. Retry please!';
      },
    });
  }

  reset() {
    this.urlForm.reset(null);
    this.tinyUrl = null;
    this.longUrl = null;
    this.error = null;
  }
}
