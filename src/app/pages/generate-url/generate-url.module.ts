import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { GenerateUrlComponent } from './generate-url.component';
import { UrlInputComponent } from './url-input/url-input.component';
import { ClipboardModule } from '@angular/cdk/clipboard';

@NgModule({
  declarations: [GenerateUrlComponent, UrlInputComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    ClipboardModule,
  ],
  exports: [GenerateUrlComponent],
})
export class GenerateUrlModule {}
