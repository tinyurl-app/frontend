import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidenavModule } from './components/sidenav/sidenav.module';
import { GenerateUrlModule } from './pages/generate-url/generate-url.module';
import { PopularDomainsModule } from './pages/popular-domains/popular-domains.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SidenavModule,
    GenerateUrlModule,
    HttpClientModule,
    PopularDomainsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
