import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GenerateUrlComponent } from './pages/generate-url/generate-url.component';
import { PopularDomainsComponent } from './pages/popular-domains/popular-domains.component';

const routes: Routes = [
  {
    path: 'url',
    component: GenerateUrlComponent,
  },
  {
    path: 'domains',
    component: PopularDomainsComponent,
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'url',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
