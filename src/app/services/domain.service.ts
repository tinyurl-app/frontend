import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IDomain } from '../interfaces/domain.interface';

@Injectable({
  providedIn: 'root',
})
export class DomainService {
  constructor(private http: HttpClient) {}

  getPopularDomains(): Observable<IDomain[]> {
    const apiUrl = environment.DOMAINS;
    return this.http.get<IDomain[]>(apiUrl);
  }
}
