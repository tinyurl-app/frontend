import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { IShortenedUrl } from '../interfaces/url.interface';

@Injectable({
  providedIn: 'root',
})
export class UrlService {
  constructor(private http: HttpClient) {}

  transformUrl(url: string): Observable<string> {
    const apiUrl = environment.SHORTEN_URL;
    return this.http
      .post<IShortenedUrl>(apiUrl, { url })
      .pipe(map((res) => res && res.tinyUrl));
  }
}
